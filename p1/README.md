> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Rhianna Reichert

### Project 1 Requirements:

*Four Parts:*

1. My Online Portfolio web app
2. Chapter Questions
    - Chapters 9, 10, and 15
3. Java Skill Sets 7, 8, and 9
    - Skill Set 7: Random Array Using Methods and Data Validation
    - Skill Set 8: Largest of Three Integers
    - Skill Set 9: Array Runtime Data Validation
4. Bitbucket repo link

#### README.md file should include the following items:

* Create My Business Card! Android app
* Provide screenshots of completed app
    * (Pixel 3 and Nexus 5)
* Provide screenshots of completed Java Skill Sets
    * Skill Set 7: Random Array Using Methods and Data Validation
    * Skill Set 8: Largest of Three Integers
    * Skill Set 9: Array Runtime Data Validation

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:


*Screenshots of Android Studio Pixel 3 - My Business Card!*:

|       My Business Card! - Main         |            My Business Card! - Details             |
|:---------------------------------:|:--------------------------------------------:|
|              ![My Business Card! - Main](img/pixel_3_main.png "My Business Card! - Main")              |                    ![My Business Card! - Details](img/pixel_3_details.png "My Business Card! - Details")


*Screenshots of Java Skill Sets*:

|        Skill Set 7: Random Array Using Methods and Data Validation        |       Skill Set 8: Largest of Three Integers   |               Skill Set 9: Array Runtime Data Validation   |                                      
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:
|              ![Skill Set 7: Random Array Using Methods and Data Validation](img/ss7_random_array_data_validation.png "Skill Set 7: Random Array Using Methods and Data Validation")              |                    ![Skill Set 8: Largest of Three Integers](img/ss8_largest_of_three_integers.png "Skill Set 8: Largest of Three Integers")|              ![Skill Set 9: Array Runtime Data Validation](img/ss9_array_runtime_data_validation.png "Skill Set 9: Array Runtime Data Validation")



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
