<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Project 1: My Business Card!">
		<meta name="author" content="Rhianna N. Reichert">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Project 1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Backward-Engineer the screenshots below, using *your* photo, contact information, and interests.
				</p>
				<p class="text-justify">
					Research how to do the following requirements (see screenshots below):
					<ol align="left">
					<li>Create a launcher icon image and display it in both activities (screens)</li>
					<li>Must add background color(s) to both activities</li>
					<li>Must add border around image and button</li>
					<li>Must add text shadow (button)</li>
					<ol>
				</p><br />

				<table>
				<tr><td>
				<h4><center>Android Studio Pixel 3 - My Business Card!: Main User Interface</center></h4>
				<img src="img/pixel_3_main.png" class="img-responsive center-block" alt="Android Studio Pixel 3 - My Business Card!: Main User Interface"></td>

				<td><center><h4>Android Studio Pixel 3 - My Business Card!: Details User Interface</center></h4>
				<img src="img/pixel_3_details.png" class="img-responsive center-block" alt="Android Studio Pixel 3 - My Business Card!: Details User Interface"></td>
				</tr>
				</table><br />

				<table>
				<tr>
				<td><h4><center>Java Skillset 7: Random Array Using Methods and Data Validation</center></h4>
				<img src="img/ss7_random_array_data_validation.png" width="400px" class="img-responsive center-block" alt="Java Skillset 7: Random Array Using Methods and Data Validation"></td>

				<td><h4><center>Java Skillset 8: Largest of Three Integers</center></h4>
				<img src="img/ss8_largest_of_three_integers.png" width="300px" class="img-responsive center-block" alt="Java Skillset 8: Largest of Three Integers"></td>

				<td><h4><center>Java Skillset 9: Array Runtime Data Validation</center></h4>
				<img src="img/ss9_array_runtime_data_validation.png" width="300px" class="img-responsive center-block" alt="Java Skillset 9: Array Runtime Data Validation"></td>
				</tr>
				</table><br />
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
