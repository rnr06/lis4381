> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Rhianna Reichert

### Assignment 3 Requirements:

*Four Parts:*

1. My Events Mobile App
2. Chapter Questions (Chs 5, 6)
3. Skill Sets 4, 5, and 6
4. Bitbucket repo link

#### README.md file should include the following items:

* Create My Events Android app
* Provide screenshots of completed app
    * (Pixel 3 and Nexus 5)
* Provide screenshots of completed Java Skill Sets
    * (4, 5, and 6)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:


*Screenshots of Android Studio Pixel 3 - My Events*:

|       My Events - Main         |            My Events - Calculated             |
|:---------------------------------:|:--------------------------------------------:|
|              ![My Events - Main](img/pixel_3_main.png "My Events - Main")              |                    ![My Events - Calculated](img/pixel_3_calculated.png "My Events - Calculated")


*Screenshots of Java Skill Sets*:

|        Skill Set 4: Decision Structures - 1        |       Skill Set 4: Decision Structures - 2   |               Skill Set 4: Decision Structures - 3   |                                      
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:
|              ![Skill Set 4: Decision Structures - 1](img/ss4_decision_structures_1.png "Skill Set 4: Decision Structures - 1")              |                    ![Skill Set 4: Decision Structures - 2](img/ss4_decision_structures_2.png "Skill Set 4: Decision Structures - 2")|              ![Skill Set 4: Decision Structures - 3](img/ss4_decision_structures_3.png "Skill Set 4: Decision Structures - 3")

*Screenshots of Java Skill Sets Continued*:

|Skill Set 5: Random Array   |               Skill Set 6: Methods   |                                      
|:--------------------------------------------:|:--------------------------------------------:
|                    ![Skill Set 5: Random Array](img/ss5_random_array.png "Skill Set 5: Random Array")|              ![Skill Set 6: Methods](img/ss6_methods.png "Skill Set 6: Methods")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
