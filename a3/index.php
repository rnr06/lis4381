<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Rhianna N. Reichert">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Frequently, not only will you be asked to design and develop Web applications, but you will also be asked to create (design) database solutions that interact with the Web application—and, in fact, the data repository is the *core* of all Web applications. Hence, the following business requirements.<br /><br />
					A pet store owner, who owns a number of pet stores, requests that you develop a Web application whereby he and his team can record, track, and maintain relevant company data, based upon the following business rules:
					<ol align="left">
					<li>A customer can buy many pets, but each pet, if purchased, is purchased by only one customer.</li>
					<li>A store has many pets, but each pet is sold by only one store.</li>
					</ol>
				</p>
				<p class="text-justify">
					<strong>Remember:</strong> an organization’s business rules are the key to a well-designed database.<br /><br />
					For the Pet's R-Us business, it's important to ask the following questions to get a better idea of how the database and Web application should work together:
					<ul align="left">
					<li>Can a customer exist without a pet? Seems reasonable. Yes. (optional)</li>
					<li>Can a pet exist without a customer? Again, yes. (optional)</li>
					<li>Can a pet store not have any pets? It wouldn’t be a pet store. (mandatory)</li>
					<li>Can a pet exist without a pet store? Not in this design. (mandatory)</li>
					</ul>
				</p>
				<p class="text-justify">
					<strong>Solutions:</strong>
					<ul align="left">
					<li><a href="docs/a3.mwb">A3 MWB</a></li>
					<li><a href="docs/a3.sql">A3 SQL</a></li>
					</ul>
				</p>

				<h4>Petstore ERD</h4>
				<img src="img/petstore_erd.png" class="img-responsive center-block" alt="Petstore ERD"><br />
				
				<table>
				<tr><td>
				<h4>Android Studio Pixel 3 - My Events: Main User Interface</h4>
				<img src="img/pixel_3_main.png" class="img-responsive center-block" alt="Android Studio Pixel 3 - My Events: Main User Interface"></td>

				<td><h4>Android Studio Pixel 3 - My Events: Calculated User Interface</h4>
				<img src="img/pixel_3_calculated.png" class="img-responsive center-block" alt="Android Studio Pixel 3 - My Events: Calculated User Interface"></td>
				</tr>
				</table><br />

				<table>
				<tr><td>
				<h4>Java Skillset 4: Decision Structures - 1</h4>
				<img src="img/ss4_decision_structures_1.png" width="350" class="img-responsive center-block" alt="Java Skillset 4: Decision Structures - 1"></td>

				<td><h4>Java Skillset 4: Decision Structures - 2</h4>
				<img src="img/ss4_decision_structures_2.png" width="350" class="img-responsive center-block" alt="Java Skillset 4: Decision Structures - 2"></td>

				<td><h4>Java Skillset 4: Decision Structures - 3</h4>
				<img src="img/ss4_decision_structures_3.png" width="350" class="img-responsive center-block" alt="Java Skillset 4: Decision Structures - 3"></td>
				</tr>
				</table><br />

				<table>
				<tr><td>
				<h4>Java Skillset 5: Random Array</h4>
				<img src="img/ss5_random_array.png" width="400" class="img-responsive center-block" alt="Java Skillset 5: Random Array"></td>

				<td><h4>Java Skillset 6: Methods</h4>
				<img src="img/ss6_methods.png" widht="400" class="img-responsive center-block" alt="Java Skillset 6: Methods"></td>
				</tr>
				</table><br />
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
