> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Rhianna Reichert

### Project 2 Requirements:

*Four Parts:*

1. Modify Petstore web app to include edits
    - Turn off client-side validation by commenting out the code
    - Add server-side validation and regular expressions - as per the database entity attribute requirements
2. RSS Feed
3. Chapter Questions
    - Chapters 13 and 14
4. Bitbucket repo link

#### README.md file should include the following items:

* Modify and add to [My Online Portfolio](http://localhost/repos/lis4381/index.php "My Online Portfolio") Petstore web app
* Provide screenshots of completed app
    * index.php
    * edit_petstore.php
    * edit_petstore_process.php (that includes error.php)
* Provide screenshots of:
    * Home page carousel
    * RSS Feed

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:


*Screenshot of [My Online Portfolio](http://localhost/repos/lis4381/index.php "My Online Portfolio") - Main Page*:
![My Online Portfolio - Main Page](img/p2_my_online_portfolio_main.png "My Online Portfolio - Main Page")


*Screenshot of [Edit Pet Store](http://localhost/repos/lis4381/p2/index.php "Edit Pet Store") - index.php*:
![Edit Pet Store index.php](img/p2_index.png "Edit Pet Store index.php")


*Screenshots of My Online Portfolio - Edit Pet Store*:

|        Edit Pet Store - edit_petstore.php        |       Edit Pet Store - edit_petstore_process.php   |                                      
|:---------------------------------:|:--------------------------------------------:|
|              ![Edit Pet Store - edit_petstore.php](img/p2_edit_petstore.png "Edit Pet Store - edit_petstore.php")              |                    ![Edit Pet Store - edit_petstore_process.php](img/p2_edit_petstore_process.png "Edit Pet Store - edit_petstore_process.php")


*Screenshot of [RSS Feed](http://feeds.foxnews.com/foxnews/scitech "RSS Feed") - Fox SciTech*:
![RSS Feed - Fox SciTech](img/p2_rss_feed.png "RSS Feed - Fox SciTech")



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
