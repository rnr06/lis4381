<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title>Using RSS Feeds</title>
    </head>
    <body>
        <?php
        //Note: RSS specification: https://validator.w3.org/feed/docs/rss2.html
        
        $html = "";
        $publisher = "Fox SciTech";
        $url = "http://feeds.foxnews.com/foxnews/scitech";

        $html .= '<h3>' . $publisher . '<h3>';
        $html .= $url;

        $rss = simplexml_load_file($url);
        $count = 0;
        $html .= '<ol>';
        foreach($rss->channel->item as $item)
        {
            $count++;
            if($count > 10)
            {
                break;
            }
            $html .= '<li><a href="'.htmlspecialchars($item->link).'">'. htmlspecialchars($item->title) . '</a><br />';
            $html .= htmlspecialchars($item->description) . '<br />';
            $html .= htmlspecialchars($item->pubDate) . '</li><br />';
        }
        $html .= '</ol>';

        print $html;
        ?>
    </body>
</html>