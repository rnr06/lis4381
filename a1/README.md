> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Rhianna Reichert

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development installations
3. Chapter Questions (Chs 1, 2)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation [My PHP Installation](img/php.png "My PHP Installation");
* Screenshot of running java Hello;
* Screenshot of running Android Studio - My First App;
* git commands w/short descriptions;
* Bitbucket repo links:
    a) this assignment and
    b) the completed tutorial above (bitbucketstationlocations).

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. One additional git command: git remote - Manage set of tracked repositories

#### Assignment Screenshots:
_Screenshot of AMPPS running  [My PHP Installation](img/php.png "PHP Localhost"):_

![AMPPS Installation Screenshot](img/php.png "PHP Localhost")

|                                   |                                              |
|:---------------------------------:|:--------------------------------------------:|
| _Screenshot of running java Hello:_ | _Screenshot of Android Studio - My First App:_ |
|              ![JDK Installation Screenshot](img/jdk_install.png "JDK Installation Screenshot")              |                    ![Android Studio Installation Screenshot](img/Nexus_5_API_23_Screenshot.png "Android Studio Installation Screenshot")                   |




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
