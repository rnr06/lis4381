<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Assignment 1: Distributed Version Control Setup">
		<meta name="author" content="Rhianna N. Reichert">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> For Assignment 1 you were to setup Distributed Version Controls, install development tools (AMPPS, JDK, Android Studio), create Bitbucket repo, and create "My First App" using Android Studio. Screenshots of each requirement was also required. 
				</p><br />

				<h4>Java Installation</h4>
				<img src="img/jdk_install.png" class="img-responsive center-block" alt="JDK Installation"><br />

				<h4>Android Studio Installation</h4>
				<img src="img/Nexus_5_API_23_Screenshot.png" class="img-responsive center-block" alt="Android Studio Installation"><br />

				<h4>AMPPS Installation</h4>
				<img src="img/php.png" class="img-responsive center-block" alt="AMPPS Installation"><br />
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
