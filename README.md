> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Rhianna Reichert

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Intall AMPPS (*ONLY* if not previously installed)
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Healthy Recipes Android app
    - Provide screenshots of completed app
        * (Pixel 3 and Nexus 5)
    - Provide screenshots of completed Java Skill Sets
        * (1, 2, and 3)

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create My Events Android app
    - Provide screenshots of completed app
        * (Pixel 3 and Nexus 5)
    - Provide screenshots of completed Java Skill Sets
        * (4, 5, and 6)

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create My Online Portfolio web app
    - Provide screenshots of completed app
        * (Failed Validation and Passed Validation)
    - Provide screenshots of completed Java Skill Sets
        * Skill Set 10: Java: Array Lists
        * Skill Set 11: Java: Nested Structures
        * Skill Set 12: Java: Temperature Conversion Program

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create and add to [My Online Portfolio](http://localhost/repos/lis4381/index.php "My Online Portfolio") Petstore web app
    - Provide screenshots of completed app
        * (Failed Validation and Records Table)
    - Provide screenshots of completed Java Skill Sets
        * Skill Set 13: Java: Sphere Volume Calculator
        * Skill Set 14: PHP: Simple Calculator
        * Skill Set 15: PHP: Write/Read File

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create My Business Card! Android app
    - Provide screenshots of completed app
        * (Pixel 3 and Nexus 5)
    - Provide screenshots of completed Java Skill Sets
        * Skill Set 7: Random Array Using Methods and Data Validation
        * Skill Set 8: Largest of Three Integers
        * Skill Set 9: Array Runtime Data Validation

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Modify and add to [My Online Portfolio](http://localhost/repos/lis4381/index.php "My Online Portfolio") Petstore web app
    - Provide screenshots of completed app
        * index.php
        * edit_petstore.php
        * edit_petstore_process.php (that includes error.php)
    - Provide screenshots of:
        * Home page carousel
        * RSS Feed


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")