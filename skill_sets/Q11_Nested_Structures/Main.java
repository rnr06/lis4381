class Main
{
    public static void main(String args[])
    {
        //call static methods (i.e., no object, non-value returning)
        Methods.getRequirements();
        Methods.createArray();
    }
}