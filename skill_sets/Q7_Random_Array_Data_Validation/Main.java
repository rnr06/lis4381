class Main
{
    public static void main(String args[])
    {
        //call static methods (i.e., no object, non-value returning)
        Methods.getRequirements();
        
        //Java style String[] my Array
        //C++ style String myArray[]
        //call createArray() method in Methods class
        //returns initialized array, array size determined by user
        int[] userArray = Methods.createArray();    //Java style array

        //call generatePseudoRandomNumber() method, passing returned array above
        //prints pseudo-randomly gnerated numbers, determined by number user inpout
        Methods.generatePseudoRandomNumbers(userArray); //pass array
    }
}