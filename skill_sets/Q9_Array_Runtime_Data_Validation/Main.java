class Main
{
    public static void main(String args[])
    {
        //call static methods (i.e., no object, non-value returning)
        Methods.getRequirements();
        
        //Java style String[] my Array
        //C++ style String myArray[]
        //returns initialized array, array size determined by user
        int arraySize=0;
        arraySize = Methods.validateArraySize();    //Java style array

        //call generatePseudoRandomNumber() method, passing returned array above
        //prints pseudo-randomly gnerated numbers, determined by number user inpout
        Methods.calculateNumbers(arraySize); //pass array
    }
}