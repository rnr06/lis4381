> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Rhianna Reichert

### Assignment 4 Requirements:

*Seven Parts:*

1. Create repos subdirectory in AMPPS
2. Clone assignment starter files
3. Create a favicon
4. My Online Portfolio web app
5. Chapter Questions
    - Chapters 9, 10, and 15
6. Java Skill Sets 10, 11, and 12
    - Skill Set 10: Java: Array Lists
    - Skill Set 11: Java: Nested Structures
    - Skill Set 12: Java: Temperature Conversion Program
7. Bitbucket repo link

#### README.md file should include the following items:

* Create [My Online Portfolio](http://localhost/repos/lis4381/index.php "My Online Portfolio") web app
* Provide screenshots of completed app
    * (Failed Validation and Passed Validation)
* Provide screenshots of completed Java Skill Sets
    * Skill Set 10: Java: Array Lists
    * Skill Set 11: Java: Nested Structures
    * Skill Set 12: Java: Temperature Conversion Program

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:


*Screenshot of [My Online Portfolio](http://localhost/repos/lis4381/index.php "My Online Portfolio") - Main Page*:
![My Online Portfolio - Main Page](img/a4_my_online_portfolio_main.png "My Online Portfolio - Main Page")

*Screenshots of My Online Portfolio - Validation Pages*:

|       My Online Portfolio - Failed Validation         |            My Online Portfolio - Passed Validation             |
|:---------------------------------:|:--------------------------------------------:|
|              ![My Online Portfolio - Failed Validation](img/a4_my_online_portfolio_failed.png "My Online Portfolio - Failed Validation")              |                    ![My Online Portfolio - Passed Validation](img/a4_my_online_portfolio_passed.png "My Online Portfolio - Passed Validation")


*Screenshots of Java Skill Sets*:

|        Skill Set 10: Java: Array Lists        |       Skill Set 11: Java: Nested Structures - 1   |                                      
|:---------------------------------:|:--------------------------------------------:|
|              ![Skill Set 10: Java: Array Lists](img/ss10_array_lists.png "Skill Set 10: Java: Array Lists")              |                    ![Skill Set 11: Java: Nested Structures - 1](img/ss11_nested_structures_1.png "Skill Set 11: Java: Nested Structures - 1")

|       Skill Set 11: Java: Nested Structures - 2   |               Skill Set 12: Java: Temperature Conversion Program   |                                      
|:--------------------------------------------:|:--------------------------------------------:
|                    ![Skill Set 11: Java: Nested Structures - 2](img/ss11_nested_structures_2.png "Skill Set 11: Java: Nested Structures - 2")              |                    ![Skill Set 12: Java: Temperature Conversion Program](img/ss12_temperature_conversion_program.png "Skill Set 12: Java: Temperature Conversion Program")



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
