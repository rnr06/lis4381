> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Rhianna Reichert

### Assignment 5 Requirements:

*Four Parts:*

1. Create and add Petstore web app
2. Chapter Questions
    - Chapters 11, 12, and 19
3. Java Skill Sets 13, 14, and 15
    - Skill Set 13: Java: Sphere Volume Calculator
    - Skill Set 14: PHP: Simple Calculator
    - Skill Set 15: PHP: Write/Read File
4. Bitbucket repo link

#### README.md file should include the following items:

* Create and add to [My Online Portfolio](http://localhost/repos/lis4381/index.php "My Online Portfolio") Petstore web app
* Provide screenshots of completed app
    * (Failed Validation and Records Table)
* Provide screenshots of completed Java Skill Sets
    * Skill Set 13: Java: Sphere Volume Calculator
    * Skill Set 14: PHP: Simple Calculator
    * Skill Set 15: PHP: Write/Read File

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:


*Screenshot of [My Online Portfolio](http://localhost/repos/lis4381/index.php "My Online Portfolio") - Main Page*:
![My Online Portfolio - Main Page](img/a5_my_online_portfolio_main.png "My Online Portfolio - Main Page")

*Screenshots of My Online Portfolio - Add Pet Store*:

|       My Online Portfolio - Failed Validation         |            My Online Portfolio - Records Table             |
|:---------------------------------:|:--------------------------------------------:|
|              ![My Online Portfolio - Failed Validation](img/a5_add_petstore_process_error.png "My Online Portfolio - Failed Validation")              |                    ![My Online Portfolio - Records Table](img/a5_index.png "My Online Portfolio - Records Table")


*Screenshots of Java Skill Sets*:

Skill Set 13: Java: Sphere Volume Calculator ![Skill Set 13: Java: Sphere Volume Calculator](img/ss13_sphere_volume_calculator.png "Skill Set 13: Java: Sphere Volume Calculator")

|        Skill Set 14: PHP: Simple Calculator - Addition        |       Skill Set 14: PHP: Simple Calculator - Addition Process   |                                      
|:---------------------------------:|:--------------------------------------------:|
|              ![Skill Set 14: PHP: Simple Calculator - Addition](img/ss14_simple_calculator_add.png "Skill Set 14: PHP: Simple Calculator - Addition")              |                    ![Skill Set 14: PHP: Simple Calculator - Addition Process](img/ss14_simple_calculator_add_process.png "Skill Set 14: PHP: Simple Calculator - Addition Process")

|       Skill Set 14: PHP: Simple Calculator - Division by Zero   |               Skill Set 14: PHP: Simple Calculator - Division by Zero Process   |                                      
|:--------------------------------------------:|:--------------------------------------------:
|                    ![Skill Set 14: PHP: Simple Calculator - Division by Zero](img/ss14_simple_calculator_div_by_zero.png "Skill Set 14: PHP: Simple Calculator - Division by Zero")              |                    ![Skill Set 14: PHP: Simple Calculator - Division by Zero Process](img/ss14_simple_calculator_div_by_zero_process.png "Skill Set 14: PHP: Simple Calculator - Division by Zero Process")

|       Skill Set 15: PHP: Write/Read File - Index   |               Skill Set 15: PHP: Write/Read File - Process   |                                      
|:--------------------------------------------:|:--------------------------------------------:
|                    ![Skill Set 15: PHP: Write/Read File - Index](img/ss15_write_read_file_index.png "Skill Set 15: PHP: Write/Read File - Index")              |                    ![Skill Set 15: PHP: Write/Read File - Process](img/ss15_write_read_file_process.png "Skill Set 15: PHP: Write/Read File - Process")



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
