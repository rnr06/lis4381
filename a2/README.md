> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Rhianna Reichert

### Assignment 2 Requirements:

*Four Parts:*

1. Healthy Recipe Mobile App
2. Chapter Questions (Chs 3, 4)
3. Skill Sets 1, 2, and 3
4. Bitbucket repo link

#### README.md file should include the following items:

* Create Healthy Recipes Android app
* Provide screenshots of completed app
    * (Pixel 3 and Nexus 5)
* Provide screenshots of completed Java Skill Sets
    * (1, 2, and 3)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:


*Screenshots of Android Studio Pixel 3 - Healthy Recipes*:

|       Healthy Recipes - Main         |            Healthy Recipes - Recipe             |
|:---------------------------------:|:--------------------------------------------:|
|              ![Healthy Recipes - Main](img/pixel_3_main.png "Healthy Recipes - Main")              |                    ![Healthy Recipes - Recipe](img/pixel_3_recipe.png "Healthy Recipes - Recipe")


*Screenshots of Android Studio Nexus 5 - Healthy Recipes*:

|       Healthy Recipes - Main         |            Healthy Recipes - Recipe             |
|:---------------------------------:|:--------------------------------------------:|
|              ![Healthy Recipes - Main](img/nexus_5_main.png "Healthy Recipes - Main")              |                    ![Healthy Recipes - Recipe](img/nexus_5_recipe.png "Healthy Recipes - Recipe")


*Screenshots of Java Skill Sets*:

|        Skill Set 1: Even Or Odd        |       Skill Set 2: Largest of Two Integers   |               Skill Set 3: Arrays and Loops   |                                      
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:
|              ![Skill Set 1: Even Or Odd](img/ss1_even_or_odd.png "Skill Set 1: Even Or Odd")              |                    ![Skill Set 2: Largest of Two Integers](img/ss2_largest_number.png "Skill Set 2: Largest of Two Integers")|              ![Skill Set 3: Arrays and Loops](img/ss3_arrays_and_loops.png "Skill Set 3: Arrays and Loops")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
