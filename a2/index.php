<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Assignment 2 - Mobile App: Healthy Recipes">
		<meta name="author" content="Rhianna N. Reichert">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> For Assignment 2 you were to a mobile recipe app using Android Studio called "Healthy Recipes." You were also to complete three Java Skillsets (Skillset 1: Even Or Odd, Skillset 2: Largest of Two Integers, and Skillset 3: Arrays and Loops). Screenshots of each user interface were required. 
				</p><br />

				<table>
				<tr><td>
				<h4>Android Studio Pixel 3 - Healthy Recipes: Main User Interface</h4>
				<img src="img/pixel_3_main.png" class="img-responsive center-block" alt="Android Studio Pixel 3 - Healthy Recipes: Main User Interface"></td>

				<td><h4>Android Studio Pixel 3 - Healthy Recipes: Recipe User Interface</h4>
				<img src="img/pixel_3_recipe.png" class="img-responsive center-block" alt="Android Studio Pixel 3 - Healthy Recipes: Recipe User Interface"></td>
				</tr>
				<tr><td>
				<h4>Android Studio Nexus 5 - Healthy Recipes: Recipe User Interface</h4>
				<img src="img/nexus_5_recipe.png" class="img-responsive center-block" alt="Android Studio Nexus 5 - Healthy Recipes: Recipe User Interface"></td>

				<td><h4>Android Studio Nexus 5 - Healthy Recipes: Recipe User Interface</h4>
				<img src="img/nexus_5_recipe.png" class="img-responsive center-block" alt="Android Studio Nexus 5 - Healthy Recipes: Recipe User Interface"></td>
				</tr>
				</table><br />

				<table>
				<tr><td>
				<h4>Java Skillset 1: Even Or Odd</h4>
				<img src="img/ss1_even_or_odd.png" class="img-responsive center-block" alt="Java Skillset 1: Even Or Odd"></td>

				<td><h4>Java Skillset 2: Largest of Two Integers</h4>
				<img src="img/ss2_largest_number.png" class="img-responsive center-block" alt="Java Skillset 2: Largest of Two Integers"></td>

				<td><h4>Java Skillset 3: Arrays and Loops</h4>
				<img src="img/ss3_arrays_and_loops.png" class="img-responsive center-block" alt="Java Skillset 3: Arrays and Loops"></td>
				</tr>
				</table><br />
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
